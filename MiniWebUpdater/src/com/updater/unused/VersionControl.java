/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.updater.unused;

import com.updater.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VersionControl 
{
    /**
     * Get my highest version from files in cacheDir
     * @return highest version
     */
    public static double getMyHighestVersion() 
    {
        File[] listOfFiles = Constants.cacheDir.listFiles(); // List of all files in cacheDir
        
        if (listOfFiles.length == 0) 
        { // Directory is empty
            System.out.println(Constants.cacheDir.getAbsolutePath() + " is empty");
            return -0.0;
        }
        
        List<Double> versionList = new ArrayList(); // Store all current file versions in cacheDir to array
        for (File file : listOfFiles) 
        {
            if (file.isFile()) 
            {
            Matcher matcher = Pattern.compile("(\\d\\.\\d)").matcher(file.getName()); // Get the double value of the file name
            matcher.find(); // Find the double
            double version = Double.parseDouble(matcher.group(1)); // Version found
            versionList.add(version); // Add version to versionList
            }
           }
            double highestVersion = Collections.max(versionList); // Get the highest version from versionList using Collections
            return highestVersion;
    }
    
    /**
     * Get the double value of a file
     * @param file
     * @return double value, version
     */
    public static double getVersionFromString(String s) 
    {
        Matcher matcher = Pattern.compile("(\\d\\.\\d)").matcher(s); // Get the double value of the file name
        matcher.find(); // Find the double
        double version = Double.parseDouble(matcher.group(1)); // Version found
        return version;
    }
    
    /**
     * compare two versions
     * @param versionA
     * @param versionB
     * @return highestVersion
     */
    public static double compareVersion(double versionA, double versionB) 
    {
       Collection<Double> collection = Collections.emptyList();
       collection.add(versionA);
       collection.add(versionB);
       double highestVersion = Collections.max(collection); // Get the highest version from collection using Collections
       return highestVersion;
    }
}
