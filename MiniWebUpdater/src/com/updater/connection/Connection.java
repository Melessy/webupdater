/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.updater.connection;

import com.updater.Constants;
import com.updater.checksums.Checksum;
import com.updater.checksums.ChecksumHandler;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class Connection 
{
    private final String[] hostArray = {"merdfd.duckdns.org", "melessy.duckdns.org", "melds.duckdns.org", "melkova.duckdns.org", "127.0.0.1"};
    private Links links;
    
    /**
     * Get available checksum & download links
     * @return Links
     * @throws java.net.URISyntaxException
     * @see Links.class
     */
    public Links getAvailableLinks() throws URISyntaxException 
    {
         for (String host : hostArray) 
        { // Iterate through hostArray
            String rootLink = String.format("http://"+host+":8080/content/updaterCache/"); // Root link
            String checksumLink = String.format(rootLink + "Checksums.json"); // Link to check
            if (!isAvailableLink(checksumLink)) 
            { // Check if checksum link is available
              Constants.logger.log(Level.WARNING, "Checksum link unavailable {0}", checksumLink);
              //continue; // Continue to check other checksumLink if not available
            } 
            else 
            {
            Constants.logger.log(Level.INFO, "Checksum link available {0}", checksumLink);
            ChecksumHandler checksumHandler = new ChecksumHandler(checksumLink); // Initialize checksumHandler
            List<Checksum> checksumList = checksumHandler.getChecksumList(); // Get checksums list
            Map<String, String> downloadLinksMap = new HashMap(); // Initialize downloadLinks map
            
            for (Checksum checksum : checksumList) 
            { // Iterate over all checksums in the checksumList
                Constants.logger.log(Level.INFO, checksum.toString());
                String downloadLink = String.format(rootLink + checksum.getName()); // Set downloadLink using fileName from checksum
            if (!isAvailableLink(downloadLink)) 
            { // Check for available downloadLink
                Constants.logger.log(Level.WARNING, "Download link unavailable {0}", downloadLink);  
            }
            else 
            {
                Constants.logger.log(Level.INFO, "Download link available {0}", downloadLink);
                String fileName = Paths.get(new URI(downloadLink).getPath()).getFileName().toString();
                downloadLinksMap.put(fileName, downloadLink); // Add available downloadLink to downloadLinks map
            }
            }
            
            if (downloadLinksMap.isEmpty()) 
            { // Return null when no download link is available
                Constants.logger.log(Level.SEVERE, "Error no download link found!");
                return null;
            }
            
         Constants.logger.log(Level.INFO, "Building links...");
         links = new Links.LinksBuilder()
                .checksumLink(checksumLink)
                .downloadLinks(downloadLinksMap)
                .build();
            return links;
      }}
        return null;
}
        
   /**
    * Check if link is available
    * @param link
    * @return HttpURLConnection.HTTP_OK
    */
   public boolean isAvailableLink(String link)
   {
        try 
        {
            URL url = new URL(link); // Set the url using String link
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.0.13) Gecko/2009073021 Firefox/3.0.13");
            int responseCode = connection.getResponseCode();
            //System.out.println("responseCode: " + responseCode);
            return responseCode == HttpURLConnection.HTTP_OK;
        } 
        catch (MalformedURLException ignore) 
        {
        } 
        catch (IOException ignore) 
        {
        }
        return false;
}
}
