/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.updater.connection;

import com.updater.Constants;
import com.updater.checksums.Checksum;
import com.updater.checksums.ChecksumHandler;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;

public class UpdateHandler 
{
    /**
     * check and do update
     * @param links
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     * @throws java.net.MalformedURLException 
     * @throws java.net.URISyntaxException 
     */
    public static void update(Links links) throws NoSuchAlgorithmException, IOException, MalformedURLException, URISyntaxException 
    {
       Constants.logger.log(Level.INFO, "Checking for updates: {0}", links.getChecksumLink());
       ChecksumHandler checksumHandler = new ChecksumHandler(links.getChecksumLink()); // Initialize checksumHandler
       List<Checksum> checksumList = checksumHandler.getChecksumList(); // Get checksums list
       for (Checksum checksum : checksumList) 
       { // Iterate over all checksums in the checksumList
         String downloadLink = (String) links.getDownloadLinks().get(checksum.getName());
         File myFile = new File(Constants.cacheDir + File.separator + checksum.getName());
         
         if (!myFile.exists()) 
          {
           Constants.logger.log(Level.INFO, "Update available for: {0}", checksum.getName());
           Downloader.download(downloadLink);
          } 
         
         String myFileHash = sha256sum(myFile);
         if (myFileHash == null ? checksum.getSha256sum() == null : myFileHash.equals(checksum.getSha256sum())) 
         { // Verify hash 
          Constants.logger.log(Level.INFO, "No update available for: {0}", myFile.getAbsolutePath());
          Constants.logger.log(Level.INFO, "Vefified hash: {0} -> {1} == {2}", new Object[]{myFile.getAbsolutePath(), checksum.getSha256sum(), myFileHash});
         } 
         else 
         {
          Constants.logger.log(Level.WARNING, "Invalid hash: {0} -> {1} != {2}", new Object[]{myFile.getAbsolutePath(), checksum.getSha256sum(), myFileHash});
          myFile.delete();
          Downloader.download(downloadLink);
       }
         
          if (checksum.isExecute()) 
          { // Execute the file
                  Constants.logger.log(Level.INFO, "Executing file: {0}", myFile.getAbsolutePath());
                  Desktop.getDesktop().open(new File(Constants.cacheDir + File.separator + myFile.getName()));
          }
          }
    }
    
    /*public static boolean isValidHash(String SHA256_A, String SHA256_B) 
    {
        return SHA256_A == null ? SHA256_B == null : SHA256_A.equals(SHA256_B);
    }*/
       
     /**
      * Get 256 checksum from a file
      * @param file to get sha256sum from
      * @return sha256sum of given file
      * @throws FileNotFoundException
      * @throws NoSuchAlgorithmException
      * @throws IOException 
      */
     @SuppressWarnings("empty-statement")
     public static String sha256sum(File file) throws FileNotFoundException, NoSuchAlgorithmException, IOException 
     {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[1024];
        int read = 0; 
        while ((read = fis.read(data)) != -1) 
        {
            sha256.update(data, 0, read);
        };
        byte[] hashBytes = sha256.digest();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hashBytes.length; i++) 
        {
          sb.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        String fileHash = sb.toString();
        return fileHash;
     }
}
