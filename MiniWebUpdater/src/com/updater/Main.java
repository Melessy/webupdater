/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.updater;

import com.updater.connection.Links;
import com.updater.connection.Connection;
import com.updater.connection.UpdateHandler;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main 
{
    public static void main(String[] args) throws IOException, URISyntaxException 
    { 
         if (!Constants.cacheDir.exists()) 
        { // Create cacheDir if not exists
            Constants.cacheDir.mkdir();
            Constants.logger.log(Level.INFO, "Created directory: {0}", Constants.cacheDir.getAbsolutePath());
        }
         
        Connection connection = new Connection(); // Connection instance
        Links links; // Links
        while ((links = connection.getAvailableLinks()) == null) 
        { // Loop until links becomes available
        try 
        {
            Constants.logger.log(Level.INFO, "Sleeping for 5" + 's');
            Thread.sleep(5000); // Sleep for 5 seconds
        }   
        catch (InterruptedException ex) 
        {
        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }}
        
         try 
         { // Handle updates
           UpdateHandler.update(links);
         } 
            catch (NoSuchAlgorithmException ex) 
           {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
  }
